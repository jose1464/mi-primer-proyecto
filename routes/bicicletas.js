var express = require("express");
var router = express.Router();
var bicicletaController = require("../controller/bicicleta");

router.get("/", bicicletaController.bicicleta_list);
router.get("/create", bicicletaController.bicicleta_create);
router.post("/create", bicicletaController.bicicleta_create_post);
//router.post("/pone nombre de ruta, recordar :parametro", aqui el nombre del controldor.aqui el metodo que va a manejar en el controlador);
router.post("/:id/delete", bicicletaController.bicicleta_delete_post);

router.get("/:id/update", bicicletaController.bicicleta_update);
router.post("/:id/update", bicicletaController.bicicleta_update_post);

module.exports = router;
