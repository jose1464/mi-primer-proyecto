var Bicicleta = function (id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
};

Bicicleta.prototype.toString = function () {
  return "id:" + this.id + "color: " + this.color;
};

Bicicleta.allBicis = [];

Bicicleta.add = function (aBici) {
  Bicicleta.allBicis.push(aBici);
};

var bici1 = new Bicicleta(1, "Amarilla", "urbana", [4.6727185, -74.1162476]);
var bici2 = new Bicicleta(2, "Roja", "Turismo", [4.6787185, -74.1162476]);
var bici3 = new Bicicleta(3, "Azul", "Turismo", [4.6767185, -74.1162476]);

Bicicleta.add(bici1);
Bicicleta.add(bici2);
Bicicleta.add(bici3);

Bicicleta.findByID = function (aBicisId) {
  var aBici = Bicicleta.allBicis.find((x) => x.id == aBicisId);
  if (aBici) {
    return aBici;
  } else {
    throw new Error("No Existe");
  }
};

Bicicleta.removeById = function (aBiciId) {
  //var aBicis=Bicicleta.findById(aBiciID);
  for (var i = 0; i < Bicicleta.allBicis.length; i++) {
    if (Bicicleta.allBicis[i].id == aBiciId) {
      Bicicleta.allBicis.splice(i, 1);
      break;
    }
  }
};

module.exports = Bicicleta;
