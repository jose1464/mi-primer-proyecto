var Bicicleta = require("../models/bicicleta");

exports.bicicleta_list = function (req, res) {
  res.render("bicicletas/index", { bicis: Bicicleta.allBicis });
};

exports.bicicleta_create = function (req, res) {
  res.render("bicicletas/create");
};

exports.bicicleta_create_post = function (req, res) {
  var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
  bici.ubicacion = [req.body.lat, req.body.lng];

  Bicicleta.add(bici);

  res.redirect("/bicicletas");
};

exports.bicicleta_delete_post = function (req, res) {
  Bicicleta.removeById(req.body.id);
  res.redirect("/bicicletas");
};

exports.bicicleta_update = function (req, res) {
  var aBici = Bicicleta.findByID(req.params.id);
  res.render("bicicletas/update", { aBici });
};

exports.bicicleta_update_post = function (req, res) {
  var aBici = Bicicleta.findByID(req.params.id);
  aBici.id = req.body.id;
  aBici.color = req.body.color;
  aBici.modelo = req.body.modelo;
  aBici.ubicacion = [req.body.lat, req.body.lng];

  res.redirect("/bicicletas");
};
