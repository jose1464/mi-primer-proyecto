const Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = function (req, res) {
  res.status(200).json({
    bicicletas: Bicicleta.allBicis,
  });
};

exports.bicicleta_create = function (req, res) {
  var aBici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
  aBici.ubicacion = [req.body.lat, req.body.lng];
  Bicicleta.add(aBici);
  res.status(200).json({
    bicicleta: aBici,
  });
};

exports.bicicleta_delete = function (req, res) {
  Bicicleta.removeById(req.body.id);
  res.status(204).send();
};

exports.bicicleta_update = function (req, res) {
  var aBici = Bicicleta.findByID(req.body.id);
  aBici.color = req.body.color;
  aBici.modelo = req.body.modelo;
  aBici.ubicacion = [req.body.lat, req.body.lng];
  res.status(200).json({
    bicicleta: aBici,
  });
};
