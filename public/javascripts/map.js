var mymap = L.map("mapid").setView([4.6727185, -74.1162476], 13);

var marker = L.marker([3.473408, -76.5541728]).addTo(mymap);
var marker2 = L.marker([3.473408, -76.5552403]).addTo(mymap);
var marker3 = L.marker([3.473408, -76.5492403]).addTo(mymap);
/*
L.tileLayer("https://a.tile.openstreetmap.org/${z}/${x}/${y}.png", {
  attribution: "&copy; fsdfsdf",
}).addTo(mymap);*/

//L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", {}).addTo(mymap);
L.tileLayer("https://mt1.google.com/vt/lyrs=r&x={x}&y={y}&z={z}", {}).addTo(
  mymap
);

$.ajax({
  datatype: "json",
  url: "api/bicicletas",
  success: function (result) {
    console.log(result);
    result.bicicletas.forEach(function (bici) {
      L.marker(bici.ubicacion, { title: bici.id }).addTo(mymap);
    });
  },
});
